# here we go folks, like plain old libpthread in C but now it is PYTHON!
# even tho asyncio is singlethreaded (i assume), and threads are scheduled
# by operating system kernel.

import asyncio
import time


async def query_database() -> int:
    # https://stackoverflow.com/questions/56729764/asyncio-sleep-vs-time-sleep
    await asyncio.sleep(3)
    return 555


async def sleep_in_a_background(sec: float):
    print("this is executed... for some reason...")
    time.sleep(5)
    await asyncio.sleep(sec)  # here program dies??? wtf,
    # whatever, this seems like undefined behaviour for me anyway
    # seems like if you have unfinished tasks in a queue, after
    # main exits, they will go on, until first await, then go away,
    # werid thing...
    print("this is NOT executed... for some reason...")


# Asyncs 101:
# https://stackoverflow.com/questions/1050222/what-is-the-difference-between-concurrency-and-parallelism
# Parallelism - when multiple threads can run code at the SAME time
# Concurrency - when multiple tasks can be stopped and resumed.
#
# I will focus on concurrency for now, but
#
# unlike true parallelism, concurrency can be achieved
#   in a single threaded environment.
#
# Whenever thread encounters "await", scheduler decides,
# which task to run next.
#
# Function that can be stoped and resumed is known as coroutine.


async def main():
    print("ok i need to querry database badly...")
    # task is pushed to queue, but not called yet.
    cor = asyncio.create_task(query_database())
    print("doing some work...")
    await asyncio.sleep(0.5)  # await encountered, scheculing new task..
    print("done some work, now i need what database gave (or will give) me...")
    # this will wait untill cor returns something...
    val = await cor
    print("got result from database {querry_result}!".format(querry_result=val))
    # this task is submitted to scheduler queue, but since await
    # is never called, main thread exits, and program terminates.
    _ = asyncio.create_task(sleep_in_a_background(10.0))
    _ = asyncio.create_task(sleep_in_a_background(10.0))
    print("main stopped execution")
    # prepare for ub!
    # in a real prod code it is a shit practice anyway to left pending
    # tasks in a queue before main exits, so no, dont do that.


if __name__ == "__main__":
    # "event loop" is a scheduler in a single thread environment.
    # whenever some "task" has time out, scheduler saves it's stackframe
    # and goes to another process.
    # unlike hardware context switch (usually cpu clock interrupts)
    # software context switch is much more agile, and used to slow operations
    # such as I/O or querrying the database.
    asyncio.run(main())
